export let coupeJson = {
    carType: 'coupe',
    carWidth: 0,
    carHeight: 0,
    placeWidth: 0,
    placeHeight: 0,
    placeCount: 0,
    parameters: {
        sectionCount: 0,
        sectionWidth: 0,
        sectionHeight: 0,
        sectionDirection: null,
        sections: []
    },
    toilets: [],
    entrances: [],
    buffet: {}
}