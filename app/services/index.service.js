import jsdom from 'jsdom'
import * as d3 from 'd3';
import fs from 'fs'
import path from 'path';
import { fileURLToPath } from 'url';
import { coupeJson } from '../jsons/coupe.js';
const { JSDOM } = jsdom;

let resultJson = {}

class AdditionalElements{
    constructor(wDifference, hDifference, x, y, startPosition, placeWidth, placeHeight, pWDifference, pHDifference, lastEndFirstAreaWidth) {
        this.wDifference = wDifference;
        this.hDifference = hDifference;
        this.x = x;
        this.y = y;
        this.startPosition = startPosition;
        this.placeWidth = placeWidth;
        this.placeHeight = placeHeight;
        this.pWDifference = pWDifference;
        this.pHDifference = pHDifference
        this.lastEndFirstAreaWidth = lastEndFirstAreaWidth;
    }

    getToilets(svg, toilets, sectionWidth, sectionHeight){
        let resultToilets = []
        for(let toilet of toilets){
            let xPos = toilet.xPosition == 'right' ? this.x - this.wDifference - this.placeWidth : this.startPosition + this.wDifference;
            let yPos = toilet.yPosition == 'top' ? this.startPosition + this.hDifference : this.y - this.hDifference - this.placeHeight; 
            
            svg.append('rect').attr('x', xPos).attr('y', yPos).attr('width', this.placeWidth).attr('height', this.placeHeight).style('stroke', 'black').style('fill', 'transparent');
            svg.append('text').text(`WC`).attr('x', xPos + this.placeWidth/4).attr('y', yPos + this.placeHeight/2).attr("font-size", `${10}px`).attr("fill", "black")
            resultToilets.push({
                xPosition: xPos,
                yPosition: yPos
            })
        }

        //fill Json
        // resultJson.toilets = resultToilets;
        return svg;
    }

    getEntrances(svg, entrances){
        let resultEntrances = [];
        for(let entrance of entrances){
            let xPos = entrance.xPosition == 'right' ? this.x - 2*this.wDifference - 50 : this.startPosition + 2*this.wDifference;
            let yPos = entrance.yPosition == 'top' ? this.startPosition : this.y - this.hDifference;
            resultEntrances.push({
                xPosition: xPos,
                yPosition: yPos
            })
            svg.append('rect').attr('x', xPos).attr('y', yPos).attr('width', 50).attr('height', 10).style('stroke', 'black').style('fill', 'transparent');
        }

        //fill Json
        // resultJson.entrances = resultEntrances;
        return svg;
    }

    getAdditionalPlaces(svg, additionalPlaceCount, currentPlaceNumeration){
        if(additionalPlaceCount <= 0 || additionalPlaceCount > 2) return svg;

        if(additionalPlaceCount == 1){
            let xPos = this.startPosition + this.lastEndFirstAreaWidth - this.wDifference - this.placeWidth;
            let yPos = this.startPosition + this.hDifference + this.placeHeight/2;
            svg.append('rect').attr('x', xPos).attr('y', yPos).attr('width', this.placeWidth).attr('height', this.placeHeight).style('stroke', 'black').style('fill', 'transparent');
            svg.append('text').text(`${currentPlaceNumeration}`).attr('x', xPos + this.placeWidth/2 - 5).attr('y', yPos + this.placeHeight/2 + 5).attr("font-size", `${10}px`).attr("fill", "black")
            return svg;
        }

        for(let i=0;i<additionalPlaceCount;i++){
            let xPos = this.startPosition + this.lastEndFirstAreaWidth - this.wDifference - this.placeWidth;
            let yPos = 0;
            if(i%2 == 0) yPos = this.startPosition + this.hDifference + this.placeHeight + this.pHDifference;
            else yPos = this.startPosition + this.hDifference;

            svg.append('rect').attr('x', xPos).attr('y', yPos).attr('width', this.placeWidth).attr('height', this.placeHeight).style('stroke', 'black').style('fill', 'transparent');
            svg.append('text').text(`${currentPlaceNumeration}`).attr('x', xPos + this.placeWidth/2 - 5).attr('y', yPos + this.placeHeight/2 + 5).attr("font-size", `${10}px`).attr("fill", "black")
            currentPlaceNumeration++;
        }

        //fil JSON

        return svg;
    }

    getSectionForConductor(svg, placeFloor){
        let xPos =  this.startPosition + this.lastEndFirstAreaWidth - 2*this.wDifference - this.placeWidth;
        let yPos = this.startPosition;
        svg.append('rect').attr('x', xPos).attr('y', yPos).attr('width', this.placeWidth + 2*this.wDifference).attr('height', this.placeHeight*placeFloor + 2*this.hDifference + (placeFloor - 1)*this.pHDifference).style('stroke', 'blue').style('fill', 'transparent');
        return svg;
    }

    getBuffet(svg, sectionWidth, sectionHeight){
        let xPos = this.x - this.lastEndFirstAreaWidth - 2*sectionWidth;
        let yPos = this.startPosition;
        svg.append('rect').attr('x', xPos).attr('y', yPos).attr('width', 2*sectionWidth).attr('height', sectionHeight).style('stroke', 'red').style('fill', 'white');

        //fill JSON
        // resultJson.buffet = {
        //     xPosition: xPos,
        //     yPosition: yPos,
        //     width: 2*sectionWidth,
        //     height: sectionHeight
        // }
        return svg
    }
}

class Places{
    constructor(placeWidth, placeHeight, placeCount, placeFloor, startPlaceDirection, wDifference, hDifference, countPlaceOfSection, pWDifference, pHDifference){
        this.placeWidth = placeWidth;
        this.placeHeight = placeHeight;
        this.placeCount = placeCount;
        this.placeFloor = placeFloor;
        this.startPlaceDirection = startPlaceDirection;
        this.wDifference = wDifference;
        this.hDifference = hDifference;
        this.countPlaceOfSection = countPlaceOfSection;
        this.currentPlaceCount = this.placeCount;
        this.currentNumeration = 1;
        this.pWDifference = pWDifference;
        this.pHDifference = pHDifference;
    }

    getSectionPlaces(svg, sectionX, sectionY, sectionNumber, sectionWidth, sectionHeight){
        let places = [];
        let countPutPlace = this.currentPlaceCount >= this.countPlaceOfSection ? this.countPlaceOfSection : this.currentPlaceCount < 0 ? 0 : this.currentPlaceCount;

        let firstPlaceCoordination = this.getFirstCoordinationOfPlace(sectionNumber, sectionX, sectionY, sectionWidth, sectionHeight);
        
        for(let i=1;i<=countPutPlace;i++){
            let coordination = {
                placeX: null,
                placeY: null
            }

            if(this.placeFloor == 3) coordination = this.getPlaceCoordinationsWhenThreeFloor(i, firstPlaceCoordination.placeX, firstPlaceCoordination.placeY);
            else if(this.placeFloor == 1) coordination = this.getPlaceCoordinationsOneFloor(i, firstPlaceCoordination.placeX, firstPlaceCoordination.placeY);
            else coordination = this.getPlaceCoordinationsTwoFloor(i, firstPlaceCoordination.placeX, firstPlaceCoordination.placeY);

            if(coordination.placeX && coordination.placeY){
                svg.append('rect').attr('x', coordination.placeX).attr('y', coordination.placeY).attr('width', this.placeWidth).attr('height', this.placeHeight).style('stroke', 'black').style('fill', 'transparent');
                svg.append('text').text(`${this.currentNumeration}`).attr('x', coordination.placeX + this.placeWidth/2 - 5).attr('y', coordination.placeY + this.placeHeight/2 + 5).attr("font-size", `${10}px`).attr("fill", "black")
            }

            //fill Json
            places.push({
                id: this.currentNumeration,
                xPosition: coordination.placeX,
                yPosition: coordination.placeY
            })

            this.currentNumeration++;
        }
        this.currentPlaceCount = this.currentPlaceCount - this.countPlaceOfSection
        return {svg, places};
    }

    getFirstCoordinationOfPlace(sectionNumber, sectionX, sectionY, sectionWidth, sectionHeight){
        let placeX = null;
        if(this.countPlaceOfSection == 2 && this.placeFloor == 2){
            if(this.startPlaceDirection == 'right'){
                if(sectionNumber%2 == 0) placeX = sectionX + this.wDifference;
                else placeX = sectionX + sectionWidth - this.wDifference - this.placeWidth;
            }else{
                if(sectionNumber%2 == 0) placeX = sectionX + sectionWidth - this.wDifference - this.placeWidth;
                else placeX = sectionX + this.wDifference;
            }
        }else{
            if(this.startPlaceDirection == 'right')  placeX = sectionX + sectionWidth - this.wDifference - this.placeWidth;
            else placeX = sectionX + this.wDifference;
        }
        return{
            placeX,
            placeY: sectionY + sectionHeight - this.hDifference - this.placeHeight
        }
    }

    getPlaceCoordinationsOneFloor(order, firstPlaceXPosition, firstPlaceYPosition){
        let coordination = {
            placeX: null,
            placeY: null
        }
        if(order == 1){
            coordination.placeX = firstPlaceXPosition,
            coordination.placeY = firstPlaceYPosition
        }else if(order == 2){
            coordination.placeX = this.startPlaceDirection == 'right' ? firstPlaceXPosition - this.placeWidth - this.pWDifference : firstPlaceXPosition + this.placeWidth + this.pWDifference,
            coordination.placeY = firstPlaceYPosition
        }
        return coordination;
    }

    getPlaceCoordinationsTwoFloor(order, firstPlaceXPosition, firstPlaceYPosition){
        let coordination = {
            placeX: null,
            placeY: null
        }
        if(order == 1){
            coordination.placeX = firstPlaceXPosition,
            coordination.placeY = firstPlaceYPosition
        }else if(order == 2){
            coordination.placeX = firstPlaceXPosition,
            coordination.placeY = firstPlaceYPosition - this.placeHeight - this.pHDifference
        }else if(order == 3){
            coordination.placeX = this.startPlaceDirection == 'right' ? firstPlaceXPosition - this.placeWidth - this.pWDifference : firstPlaceXPosition + this.placeWidth + this.pWDifference,
            coordination.placeY = firstPlaceYPosition
        }else if(order == 4){
            coordination.placeX = this.startPlaceDirection == 'right' ? firstPlaceXPosition - this.placeWidth - this.pWDifference : firstPlaceXPosition + this.placeWidth + this.pWDifference,
            coordination.placeY = firstPlaceYPosition - this.placeHeight - this.pHDifference
        }
        return coordination;
    }

    getPlaceCoordinationsWhenThreeFloor(order, firstPlaceXPosition, firstPlaceYPosition){
        let coordination = {
            placeX: null,
            placeY: null
        }
        if(order == 1){
            coordination.placeX = firstPlaceXPosition,
            coordination.placeY = firstPlaceYPosition
        }else if(order == 2){
            coordination.placeX = firstPlaceXPosition,
            coordination.placeY = firstPlaceYPosition - this.placeHeight - this.pHDifference
        }else if(order == 3){
            coordination.placeX = firstPlaceXPosition,
            coordination.placeY = firstPlaceYPosition - 2*this.placeHeight - 2*this.pHDifference
        }else if(order == 4){
            coordination.placeX = this.startPlaceDirection == 'right' ? firstPlaceXPosition - this.placeWidth - this.pWDifference : firstPlaceXPosition + this.placeWidth + this.pWDifference
            coordination.placeY = firstPlaceYPosition
        }else if(order == 5){
            coordination.placeX = this.startPlaceDirection == 'right' ? firstPlaceXPosition - this.placeWidth - this.pWDifference : firstPlaceXPosition + this.placeWidth + this.pWDifference;
            coordination.placeY = firstPlaceYPosition - this.placeHeight - this.pHDifference;
        }else if(order == 6){
            coordination.placeX = this.startPlaceDirection == 'right' ? firstPlaceXPosition - this.placeWidth - this.pWDifference : firstPlaceXPosition + this.placeWidth + this.pWDifference;
            coordination.placeY = firstPlaceYPosition - 2*this.placeHeight - 2*this.pHDifference
        }
        return coordination
    }

}


class Section{
    constructor(startPosition, startSectionDirection, places, sectionCount, lastEndFirstAreaWidth){
        this.places = places;
        this.sectionWidth = 2*this.places.placeWidth + 2*this.places.wDifference + this.places.pWDifference;
        this.sectionHeight = this.places.placeHeight*this.places.placeFloor + 2*this.places.hDifference + (this.places.placeFloor - 1)*this.places.pHDifference;
        this.startPosition = startPosition;
        this.startSectionDirection = startSectionDirection;
        this.sectionCount = sectionCount;
        this.lastEndFirstAreaWidth = lastEndFirstAreaWidth;
    }

    getCarSection(svg, x, y, carType){
        console.log(x, y, this.lastEndFirstAreaWidth , carType)
        let firstSectionXPosition = this.startSectionDirection == 'right' ? x - this.lastEndFirstAreaWidth : this.startPosition + this.lastEndFirstAreaWidth;
        let sections = [];

        for(let i=1;i<=this.sectionCount;i++){
            let sectionX = this.startSectionDirection == 'right' ? firstSectionXPosition - i*this.sectionWidth : firstSectionXPosition + (i-1)*this.sectionWidth;
            let sectionY = this.startPosition;

            // console.log(sectionX, sectionY)

            if(carType == 'plats') svg.append('rect').attr('x', sectionX).attr('y', sectionY).attr('width', 1).attr('height', this.sectionHeight).style('stroke', 'blue')
            else svg.append('rect').attr('x',sectionX).attr('y', sectionY).attr('width', this.sectionWidth).attr('height', this.sectionHeight).style('stroke', 'blue').style('fill', 'transparent');
            
            //define text x and y position
            let xText = sectionX + this.sectionWidth/2 - this.places.pWDifference/4
            let yText = sectionY + this.sectionHeight/2 - this.places.pHDifference/8
            svg.append('text').text(`${this.romanize(i)}`).attr('x', xText).attr('y', yText).attr("font-size", `${10}px`).attr("fill", "black")

            let resultPlaces = this.places.getSectionPlaces(svg, sectionX, sectionY, i, this.sectionWidth, this.sectionHeight);
            svg = resultPlaces.svg;

            sections.push({
                id: i,
                xPosition: sectionX,
                yPosition: sectionY,
                places: resultPlaces.places
            })
        }

        if(carType == 'plats')svg.append('rect').attr('x', x - this.lastEndFirstAreaWidth).attr('y', this.startPosition).attr('width', 1).attr('height', this.sectionHeight).style('stroke', 'blue')

        //fill json
        // resultJson.parameters.sectionCount = this.sectionCount;
        // resultJson.parameters.sectionDirection = this.startSectionDirection;
        // resultJson.parameters.sectionWidth = this.sectionWidth;
        // resultJson.parameters.sectionHeight = this.sectionHeight;
        // resultJson.parameters.sections = sections;
        return svg;
    }

    romanize(num) {
        var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},roman = '',i;
        for ( i in lookup ) {
          while ( num >= lookup[i] ) {
            roman += i;
            num -= lookup[i];
          }
        }
        return roman;
    }
}

class BuilderService{
    constructor() {
        this.dom = new JSDOM(`<!DOCTYPE html><body></body>`);
        this.startPosition = 10;
    }

    async generate(payload){

        // resultJson = payload.carType == 'coupe' ? coupeJson : null;

        const wDifference = 10;
        const hDifference = 10;
        const pWDifference = 20;
        const pHDifference = 20;
        const widhtForFirstAndLastArea = 40

        const places = new Places(payload.placeWidth, payload.placeHeight, payload.placeCount, payload.placeFloor, payload.startPlaceDirection, wDifference, hDifference, payload.countPlaceInSection, pWDifference, pHDifference);
        const lastEndFirstAreaWidth = places.placeWidth*2 + pWDifference + 2*wDifference + widhtForFirstAndLastArea;

        const section = new Section(this.startPosition, payload.startSectionDirection, places, payload.sectionCount, lastEndFirstAreaWidth);

        let x = payload.isHasBuffet ? 
            payload.sectionCount*section.sectionWidth + 2*lastEndFirstAreaWidth + this.startPosition + 2*section.sectionWidth : 
            payload.sectionCount*section.sectionWidth + 2*lastEndFirstAreaWidth + this.startPosition;

        let y = section.sectionHeight + (payload.carType == 'coupe' ? section.sectionHeight/2 :  section.sectionHeight) + this.startPosition;

        //fil Json
        // resultJson.carWidth = x;
        // resultJson.carHeight = y;
        // resultJson.placeCount = payload.placeCount;
        // resultJson.placeWidth = payload.placeWidth;
        // resultJson.placeHeight= payload.placeHeight;

        let body = d3.select(this.dom.window.document.querySelector("body"))
        let svg = body.append('svg').attr('width', x).attr('height', y).attr('xmlns', 'http://www.w3.org/2000/svg');
        svg = this.getCarForm(svg, x ,y);
        svg = section.getCarSection(svg, x, y, payload.carType);

        const additionalElements = new AdditionalElements(wDifference, hDifference, x, y, this.startPosition, payload.placeWidth, payload.placeHeight, pWDifference, pHDifference, lastEndFirstAreaWidth);

        svg = additionalElements.getToilets(svg, payload.toilets, section.sectionWidth, section.sectionHeight);
        svg = additionalElements.getEntrances(svg, payload.entrances);
        svg = additionalElements.getAdditionalPlaces(
            svg, 
            payload.placeCount - payload.sectionCount*payload.countPlaceInSection, 
            payload.sectionCount*payload.countPlaceInSection + 1,
        )
        if(payload.isHasSectionForConductor) svg = additionalElements.getSectionForConductor(svg, payload.placeFloor);
        if(payload.isHasBuffet && payload.carType == 'coupe') svg = additionalElements.getBuffet(svg, section.sectionWidth, section.sectionHeight);
        if(payload.isSectionHasShowerAndToilet  && payload.carType == 'coupe') console.log("HHH")

        fs.writeFileSync(this.generatePath('svg'), body.html());
        fs.writeFileSync(this.generatePath('json'), JSON.stringify(resultJson));

        return {success: true}
    }

    getCarForm(svg, x , y){
        const lineHeight = 3;
        svg.append('rect').attr('x', this.startPosition).attr('y', this.startPosition).attr('width', x).attr('height', lineHeight)
        svg.append('rect').attr('x', this.startPosition).attr('y', this.startPosition).attr('width', lineHeight).attr('height', y)
        svg.append('rect').attr('x', x-lineHeight).attr('y', this.startPosition).attr('width', lineHeight).attr('height', y)
        svg.append('rect').attr('x', this.startPosition).attr('y', y-lineHeight).attr('width', x).attr('height', lineHeight)
        return svg;
    }


    generatePath(typeFile){
        const __filename = fileURLToPath(import.meta.url);
        const __dirname = path.dirname(__filename);
        return path.resolve(__dirname, '..', '..', 'svg-keeper', typeFile == 'svg' ? 'output.svg' : 'output.json')
    }

    generateUUID() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }
}

export default BuilderService;