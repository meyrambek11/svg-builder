import express from "express"
import BuilderController from "../controllers/index.controller.js";
const router = express.Router();

const builderController = new BuilderController()
router.post('', builderController.build)

export default router;