import express from "express";
import http from "http";
import cors from "cors";
import dotenv from "dotenv"
import router from "./app/routes/index.route.js";
const app = express();

const corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
dotenv.config();

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use("/", router);

const TIMEOUT = 10 * 1000;
const server = http.createServer(app);
server.timeout = TIMEOUT;
server.setTimeout(TIMEOUT);


// set port, listen for requests
const PORT = process.env.PORT || 8080;
server.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`);
});