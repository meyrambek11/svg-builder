import BuilderService from "../services/index.service.js";

const builderService = new BuilderService()
class BuilderController{
    constructor(){}

    async build(req, res, next){
        try {
            const result = await builderService.generate(req.body)
            return res.status(200).json(result);
        } catch (e) {
            return res.status(400).json({ status: 400, message: e.message });
        }
    }
    
}

export default BuilderController;